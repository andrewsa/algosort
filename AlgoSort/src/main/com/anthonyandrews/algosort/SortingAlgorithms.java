// This class contains the five methods of sorting: bubble sort, selection sort, insertion sort,
// merge sort,
// and quick sort.

package com.anthonyandrews.algosort;

public class SortingAlgorithms {

  // used by several different methods in relation to quick sort
  private static int[] quickSortSortedArray;

  public SortingAlgorithms() {}

  public static int[] bubbleSort(int unsortedArray[]) {
    int sortedArray[] = new int[unsortedArray.length];

    for (int i = 0; i < sortedArray.length; i++) {
      sortedArray[i] = unsortedArray[i];
    }

    for (int i = (sortedArray.length - 1); i >= 0; i--) {
      for (int j = 1; j <= i; j++) {
        if (sortedArray[j - 1] > sortedArray[j]) {
          int temp = sortedArray[j - 1];
          sortedArray[j - 1] = sortedArray[j];
          sortedArray[j] = temp;
        }
      }
    }
    return sortedArray;
  }

  public static int[] selectionSort(int[] unsortedArray) {
    int sortedArray[] = new int[unsortedArray.length];

    for (int i = 0; i < sortedArray.length; i++) {
      sortedArray[i] = unsortedArray[i];
    }

    for (int i = 0; i < sortedArray.length - 1; i++) {
      int min = i;
      for (int j = i + 1; j < sortedArray.length; j++) {
        if (sortedArray[j] < sortedArray[min]) {
          min = j;
        }
        int temp = sortedArray[i];
        sortedArray[i] = sortedArray[min];
        sortedArray[min] = temp;
      }
    }
    return sortedArray;
  }

  public static int[] insertionSort(int[] unsortedArray) {
    int sortedArray[] = new int[unsortedArray.length];

    for (int i = 0; i < sortedArray.length; i++) {
      sortedArray[i] = unsortedArray[i];
    }

    for (int i = 1; i < sortedArray.length; i++) {
      int index = sortedArray[i];
      int j = i;
      while (j > 0 && sortedArray[j - 1] > index) {
        sortedArray[j] = sortedArray[j - 1];
        j--;
      }
      sortedArray[j] = index;
    }
    return sortedArray;
  }

  public static int[] mergeSort(int[] unsortedArray) {
    int sortedArray[] = new int[unsortedArray.length];
    for (int i = 0; i < sortedArray.length; i++) {
      sortedArray[i] = unsortedArray[i];
    }
    mergeSort(sortedArray, new int[sortedArray.length], 0, unsortedArray.length - 1);
    return sortedArray;
  }

  private static void mergeSort(int[] sortedArray, int[] tempArray, int left, int right) {
    int mid;
    if (right > left) {
      mid = (right + left) / 2;
      mergeSort(sortedArray, tempArray, left, mid);
      mergeSort(sortedArray, tempArray, mid + 1, right);
      merge(sortedArray, tempArray, left, mid + 1, right);
    }
  }

  private static void merge(int sortedArray[], int tempArray[], int left, int mid, int right) {
    int leftEnd;
    int numElements;
    int tempPos;
    leftEnd = mid - 1;
    tempPos = left;
    numElements = right - left + 1;

    while ((left <= leftEnd) && (mid <= right)) {
      if (sortedArray[left] <= sortedArray[mid]) {
        tempArray[tempPos] = sortedArray[left];
        tempPos = tempPos + 1;
        left++;
      } else {
        tempArray[tempPos] = sortedArray[mid];
        tempPos++;
        mid++;
      }
    }

    while (left <= leftEnd) {
      tempArray[tempPos] = sortedArray[left];
      left++;
      tempPos++;
    }

    while (mid <= right) {
      tempArray[tempPos] = sortedArray[mid];
      mid++;
      tempPos++;
    }

    for (int i = 0; i < numElements; i++) {
      sortedArray[right] = tempArray[right];
      right--;
    }
  }

  public static int[] quickSort(int[] unsortedArray) {
    quickSortSortedArray = new int[unsortedArray.length];

    for (int i = 0; i < quickSortSortedArray.length; i++) {
      quickSortSortedArray[i] = unsortedArray[i];
    }

    int left = 0;
    int right = quickSortSortedArray.length - 1;
    quickSort(left, right);

    int[] sortedArray = new int[quickSortSortedArray.length];
    for (int i = 0; i < quickSortSortedArray.length; i++) {
      sortedArray[i] = quickSortSortedArray[i];
    }

    return sortedArray;
  }

  private static void quickSort(int left, int right) {
    if (left >= right) {
      return;
    }

    int pivot = quickSortSortedArray[right];
    int partition = partition(left, right, pivot);
    quickSort(0, partition - 1);
    quickSort(partition + 1, right);
  }

  private static int partition(int left, int right, int pivot) {
    int leftCursor = left - 1;
    int rightCursor = right;

    while (leftCursor < rightCursor) {
      while (quickSortSortedArray[++leftCursor] < pivot);
      while (rightCursor > 0 && quickSortSortedArray[--rightCursor] > pivot);
      if (leftCursor >= rightCursor) {
        break;
      } else {
        swap(leftCursor, rightCursor);
      }
    }
    swap(leftCursor, right);
    return leftCursor;
  }

  private static void swap(int left, int right) {
    int temp = quickSortSortedArray[left];
    quickSortSortedArray[left] = quickSortSortedArray[right];
    quickSortSortedArray[right] = temp;
  }
}

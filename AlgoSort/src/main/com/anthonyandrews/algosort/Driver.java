// runs the sorting algorithms and provides output as to which was the fastest

package com.anthonyandrews.algosort;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.lang3.math.NumberUtils;

public class Driver {
  public static void main(String[] args) throws IOException {
    System.out.println("This program sorts an array of numbers using different methods.");
    System.out.println("Enter -h for a list of commands.");
    File file = null;
    Scanner keyboard = new Scanner(System.in);
    boolean flag = true;

    while (flag) {
      String command = keyboard.next();
      if (command.equals("-h")) {
        System.out.println("-r\tGenerate array of random size (between 10-1000 different values).");
        System.out.println("-s\tGenerate array of user-specified size (try not to go too large).");
        System.out.println("-q\tQuit.");
      } else if (command.equals("-r")) {
        file = FileRandomizer.createRandomizedFile();
        flag = false;
      } else if (command.equals("-s")) {
        System.out.println("Please enter how many values you want to sort: ");
        boolean flag2 = true;
        while (flag2) {
          String howManyValues = keyboard.next();
          if (!NumberUtils.isNumber(howManyValues)) {
            System.out.println("You must provide only a whole number!");
          } else if (Math.floor(Double.parseDouble(howManyValues)) < 1) {
            System.out.println("You must provide a value greater than 0!");
          } else {
            // accounting for possible erroneous fractional inputs
            double toBeParsed = Double.parseDouble(howManyValues);
            int toBeInput = (int) Math.floor(toBeParsed);
            file = FileRandomizer.createRandomizedFileWithSpecifiedSize(toBeInput);
            flag2 = false;
            flag = false;
          }
        }
      } else if (command.equals("-q")) {
        System.out.println("The program will now shut down.");
        System.exit(0);
      } else {
        System.out.println("Invalid input.");
      }
    }

    keyboard.close();
    ValueSorter valueSorter = new ValueSorter(file);
    valueSorter.sortWithElapsedTime();
    System.out.println("The file contained " + valueSorter.getNumberOfValues().size() + " values.");
    System.out.println("The program will now shut down.");
    file.deleteOnExit();
  }
}

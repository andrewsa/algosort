// This class creates a temp file containing randomized values to be sorted.

package com.anthonyandrews.algosort;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class FileRandomizer {
  public FileRandomizer() {}

  public static File createRandomizedFile() throws IOException {
    final File randomizedFile = File.createTempFile("randomizedFile", ".txt");
    final int lengthOfValues = new Random().nextInt(991) + 10;
    final BufferedWriter writer = new BufferedWriter(new FileWriter(randomizedFile));

    try {
      for (int i = 0; i < lengthOfValues; i++) {
        writer.write(new Random().nextInt(10001) + "");
        writer.newLine();
      }
    } finally {
      try {
        writer.close();
      } catch (Exception ignore) {
      }
    }
    return randomizedFile;
  }

  public static File createRandomizedFileWithSpecifiedSize(int howManyValues) throws IOException {
    final File randomizedFile = File.createTempFile("randomizedFile", ".txt");
    final BufferedWriter writer = new BufferedWriter(new FileWriter(randomizedFile));

    try {
      for (int i = 0; i < howManyValues; i++) {
        writer.write(new Random().nextInt(10001) + "");
        writer.newLine();
      }
    } finally {
      try {
        writer.close();
      } catch (Exception ignore) {
      }
    }
    return randomizedFile;
  }
}

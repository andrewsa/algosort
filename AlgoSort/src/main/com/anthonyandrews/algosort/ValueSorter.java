// This class populates an array with the unsorted values and then sorts them while providing
// elapsed time for each sort.

package com.anthonyandrews.algosort;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.base.Stopwatch;

public class ValueSorter {
  private File file;
  private List<Integer> unsortedValues;
  private long bubbleTime;
  private long selectionTime;
  private long insertionTime;
  private long mergeTime;
  private long quickTime;
  private String shortestTimeName;
  private int[] sortedValues;

  public ValueSorter(File file) {
    this.file = file;
  }

  public void sortWithElapsedTime() throws IOException {
    populateList();
    Stopwatch stopwatch = Stopwatch.createUnstarted();

    stopwatch.start();
    sortedValues = SortingAlgorithms
        .bubbleSort(ArrayUtils.toPrimitive(unsortedValues.toArray(new Integer[0])));
    stopwatch.stop();
    System.out.println("Bubble sort: " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " milliseconds"
        + " (" + stopwatch.elapsed(TimeUnit.NANOSECONDS) + " nanoseconds).");
    bubbleTime = stopwatch.elapsed(TimeUnit.NANOSECONDS);

    stopwatch.reset();
    stopwatch.start();
    SortingAlgorithms.selectionSort(ArrayUtils.toPrimitive(unsortedValues.toArray(new Integer[0])));
    stopwatch.stop();
    System.out.println("Selection sort: " + stopwatch.elapsed(TimeUnit.MILLISECONDS)
        + " milliseconds" + " (" + stopwatch.elapsed(TimeUnit.NANOSECONDS) + " nanoseconds).");
    selectionTime = stopwatch.elapsed(TimeUnit.NANOSECONDS);

    stopwatch.reset();
    stopwatch.start();
    SortingAlgorithms.insertionSort(ArrayUtils.toPrimitive(unsortedValues.toArray(new Integer[0])));
    stopwatch.stop();
    System.out.println("Insertion sort: " + stopwatch.elapsed(TimeUnit.MILLISECONDS)
        + " milliseconds" + " (" + stopwatch.elapsed(TimeUnit.NANOSECONDS) + " nanoseconds).");
    insertionTime = stopwatch.elapsed(TimeUnit.NANOSECONDS);

    stopwatch.reset();
    stopwatch.start();
    SortingAlgorithms.mergeSort(ArrayUtils.toPrimitive(unsortedValues.toArray(new Integer[0])));
    stopwatch.stop();
    System.out.println("Merge sort: " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " milliseconds"
        + " (" + stopwatch.elapsed(TimeUnit.NANOSECONDS) + " nanoseconds).");
    mergeTime = stopwatch.elapsed(TimeUnit.NANOSECONDS);

    stopwatch.reset();
    stopwatch.start();
    SortingAlgorithms.quickSort(ArrayUtils.toPrimitive(unsortedValues.toArray(new Integer[0])));
    stopwatch.stop();
    System.out.println("Quick sort: " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " milliseconds"
        + " (" + stopwatch.elapsed(TimeUnit.NANOSECONDS) + " nanoseconds).");
    quickTime = stopwatch.elapsed(TimeUnit.NANOSECONDS);

    shortestTimeName = determineShortestTimeName();

    System.out.println("---------\n" + shortestTimeName + " took the least amount of time.");
  }

  public List<Integer> getNumberOfValues() {
    return unsortedValues;
  }

  public String getShortestTimeName() {
    return shortestTimeName;
  }

  public int[] getSortedValues() {
    return sortedValues;
  }

  private String determineShortestTimeName() {
    Map<String, Long> unsortedTimes = new HashMap<String, Long>();
    unsortedTimes.put("Bubble sort", bubbleTime);
    unsortedTimes.put("Selection sort", selectionTime);
    unsortedTimes.put("Insertion sort", insertionTime);
    unsortedTimes.put("Merge sort", mergeTime);
    unsortedTimes.put("Quick sort", quickTime);

    Map<String, Long> sortedTimes = sortMap(unsortedTimes);
    return (String) sortedTimes.keySet().toArray()[0];
  }

  // sort via linked list in order to determine shortest time
  private Map<String, Long> sortMap(Map<String, Long> unsortedTimes) {
    List<Map.Entry<String, Long>> list =
        new LinkedList<Map.Entry<String, Long>>(unsortedTimes.entrySet());
    Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
      public int compare(Map.Entry<String, Long> o1, Map.Entry<String, Long> o2) {
        return (o1.getValue()).compareTo(o2.getValue());
      }
    });

    Map<String, Long> sortedTimes = new LinkedHashMap<String, Long>();
    for (Iterator<Map.Entry<String, Long>> it = list.iterator(); it.hasNext();) {
      Map.Entry<String, Long> entry = it.next();
      sortedTimes.put(entry.getKey(), entry.getValue());
    }
    return sortedTimes;
  }

  private void populateList() throws IOException {
    unsortedValues = new ArrayList<Integer>();
    BufferedReader reader = null;

    try {
      reader = new BufferedReader(new FileReader(file));
      String s = null;
      while ((s = reader.readLine()) != null) {
        unsortedValues.add(Integer.parseInt(s));
      }
    } finally {
      try {
        reader.close();
      } catch (Exception ignore) {
      }
    }
  }
}

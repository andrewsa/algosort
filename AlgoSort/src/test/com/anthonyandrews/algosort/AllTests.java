package com.anthonyandrews.algosort;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({FileRandomizerTest.class, SortingAlgorithmsTest.class, ValueSorterTest.class})
public class AllTests {
  public static void main(String[] args) {
    Result result = JUnitCore.runClasses(FileRandomizerTest.class, SortingAlgorithmsTest.class,
        ValueSorterTest.class);
    if (result.wasSuccessful()) {
      System.out.println("\n***All tests passed.***");
    } else {
      for (Failure failure : result.getFailures()) {
        System.out.println(failure.toString());
      }
    }
  }
}

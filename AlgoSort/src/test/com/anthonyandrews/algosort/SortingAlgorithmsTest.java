package com.anthonyandrews.algosort;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class SortingAlgorithmsTest {
  int[] unsortedArray = {4, 3, 5, 1, -2, Integer.MAX_VALUE, Integer.MIN_VALUE};

  @Test
  public void testBubbleSort() {
    int[] expected = {Integer.MIN_VALUE, -2, 1, 3, 4, 5, Integer.MAX_VALUE};
    int[] actual = SortingAlgorithms.bubbleSort(unsortedArray);
    assertTrue(Arrays.equals(expected, actual));
  }

  @Test
  public void testSelectionSort() {
    int[] expected = {Integer.MIN_VALUE, -2, 1, 3, 4, 5, Integer.MAX_VALUE};
    int[] actual = SortingAlgorithms.selectionSort(unsortedArray);
    assertTrue(Arrays.equals(expected, actual));
  }

  @Test
  public void testInsertionSort() {
    int[] expected = {Integer.MIN_VALUE, -2, 1, 3, 4, 5, Integer.MAX_VALUE};
    int[] actual = SortingAlgorithms.insertionSort(unsortedArray);
    assertTrue(Arrays.equals(expected, actual));
  }

  @Test
  public void testMergeSort() {
    int[] expected = {Integer.MIN_VALUE, -2, 1, 3, 4, 5, Integer.MAX_VALUE};
    int[] actual = SortingAlgorithms.mergeSort(unsortedArray);
    assertTrue(Arrays.equals(expected, actual));
  }

  @Test
  public void testQuickSort() {
    int[] expected = {Integer.MIN_VALUE, -2, 1, 3, 4, 5, Integer.MAX_VALUE};
    int[] actual = SortingAlgorithms.quickSort(unsortedArray);
    assertTrue(Arrays.equals(expected, actual));
  }

}

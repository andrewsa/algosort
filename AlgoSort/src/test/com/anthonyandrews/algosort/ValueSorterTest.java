package com.anthonyandrews.algosort;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

public class ValueSorterTest {
  @Test
  public void testSortWithElapsedTimeSmallListOfValues() throws IOException {
    File file = FileRandomizer.createRandomizedFileWithSpecifiedSize(5);
    ValueSorter sorter = new ValueSorter(file);
    sorter.sortWithElapsedTime();
    assertTrue((sorter.getSortedValues()[0] < sorter.getSortedValues()[1]) && (sorter.getSortedValues()[0] < sorter.getSortedValues()[sorter.getSortedValues().length - 1]));
  }

  @Test
  public void testSortWithElapsedTimeLargeListOfValues() throws IOException {
    File file = FileRandomizer.createRandomizedFileWithSpecifiedSize(999);
    ValueSorter sorter = new ValueSorter(file);
    sorter.sortWithElapsedTime();
    assertTrue((sorter.getSortedValues()[0] < sorter.getSortedValues()[1]) && (sorter.getSortedValues()[0] < sorter.getSortedValues()[sorter.getSortedValues().length - 1]));

  }

  @Test
  public void testSortWithElapsedTimeRandomListOfValues() throws IOException {
    File file = FileRandomizer.createRandomizedFile();
    ValueSorter sorter = new ValueSorter(file);
    sorter.sortWithElapsedTime();
    assertTrue((sorter.getSortedValues()[0] < sorter.getSortedValues()[1]) && (sorter.getSortedValues()[0] < sorter.getSortedValues()[sorter.getSortedValues().length - 1]));

  }
}

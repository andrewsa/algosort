package com.anthonyandrews.algosort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

public class FileRandomizerTest {

  @Test
  public void testCreateRandomizedFile() throws IOException {
    final File testFile = FileRandomizer.createRandomizedFile();
    assertTrue(testFile.exists());
    testFile.deleteOnExit();
  }

  @Test
  public void testCreateRandomizedFileWithSpecifiedSize() throws IOException {
    final File testFile = FileRandomizer.createRandomizedFileWithSpecifiedSize(5);
    assertTrue(testFile.exists());
    testFile.deleteOnExit();
  }

  @Test
  public void testCreateRandomizedFileWithSpecifiedSizeCorrectSize() throws IOException {
    final File testFile = FileRandomizer.createRandomizedFileWithSpecifiedSize(5);
    final int expected = 5;
    int actual = 0;
    final BufferedReader reader = new BufferedReader(new FileReader(testFile));
    try {
      while (reader.readLine() != null) {
        actual++;
      }
    } finally {
      try {
        reader.close();
      } catch (Exception ignore) {
      }
    }
    assertEquals(expected, actual);
    testFile.deleteOnExit();
  }
  
  @Test
  public void testCreateRandomizedFileWithSpecifiedSizeLargeSize() throws IOException {
    final File testFile = FileRandomizer.createRandomizedFileWithSpecifiedSize(9999999);
    assertTrue(testFile.exists());
    testFile.deleteOnExit();
  }
}

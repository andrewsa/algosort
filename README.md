This program sorts a file of integers using bubble sort, selection sort, insertion sort, merge sort, and quick sort, prints to console the time it took for each sorting method, and specifies which method took the least amount of time.

See ZIP for complete source code package, runnable JARs, and design doc.